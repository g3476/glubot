FROM glubo/npmjdkimage AS build

RUN mkdir /opt/app
WORKDIR /opt/app
COPY . /opt/app/
RUN ./gradlew --no-daemon clean bootJar

FROM eclipse-temurin:17-alpine AS run
RUN mkdir /opt/app
COPY --from=build /opt/app/build/libs/glubot-1.0.0-SNAPSHOT.jar /opt/app/glubot.jar
WORKDIR /opt/app

CMD ["java", "-jar", "/opt/app/glubot.jar"]
