package cz.glubo.glubot

import io.kvision.Application
import io.kvision.CoreModule
import io.kvision.BootstrapModule
import io.kvision.BootstrapCssModule
import io.kvision.html.H1
import io.kvision.html.Span
import io.kvision.html.h1
import io.kvision.html.link
import io.kvision.html.span
import io.kvision.module
import io.kvision.navbar.NavbarColor
import io.kvision.navbar.NavbarType
import io.kvision.navbar.navbar
import io.kvision.panel.VPanel
import io.kvision.panel.root
import io.kvision.panel.vPanel
import io.kvision.startApplication
import kotlinx.browser.window
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch

val AppScope = CoroutineScope(window.asCoroutineDispatcher())

class App : Application() {

    override fun start(state: Map<String, Any>) {
        val root = root("kvapp") {
        }
        AppScope.launch {
            val pingResult = Model.ping("Hello world fraom client!")
            root.vPanel {
                navbar(type = NavbarType.STICKYTOP) {
                    link(label= "Glubot",url = "#", className = "logo")
                }

                h1("adasdasd")
                span(pingResult)
            }
        }
    }
}

fun main() {
    startApplication(
        ::App,
        module.hot,
        BootstrapModule,
        BootstrapCssModule,
        CoreModule
    )
}
